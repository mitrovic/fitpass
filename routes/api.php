<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GymCardController;
use App\Http\Controllers\PasswordController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/login/check/{cardId}/{objectId}',[GymCardController::class, 'checkLogin']);

Route::get('/generatePassword/{characters}/{strength}',[PasswordController::class, 'generateStrongPassword']);
