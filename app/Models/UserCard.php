<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    use HasFactory;
    protected $table = 'user_card';

    public function getUser(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne('App\Models\Users', 'user_id');
    }
    public function getCard()
    {
        return $this->belongsTo('App\Models\Card', 'card_id');
    }
}
