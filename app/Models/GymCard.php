<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GymCard extends Model
{
    use HasFactory;
    protected $table = 'gym_card';

    public function card()
    {
        return $this->hasOne(Card::class,'id','card_id');
    }
    public function user()
    {
        return $this->hasOne(UserCard::class,'card_id','card_id');
    }
    public function gym()
    {
        return $this->hasOne(Gym::class,'id','gym_id');
    }
}
