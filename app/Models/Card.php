<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gym()
    {
        return $this->hasMany(GymCard::class, 'card_id');
    }
    public function user()
    {
        return $this->hasManyThrough(
            User::class,
            UserCard::class,
            'card_id',
            'id',
            '',
            'user_id'
        );
    }
}
