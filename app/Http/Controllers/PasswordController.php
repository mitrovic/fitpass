<?php

namespace App\Http\Controllers;

class PasswordController extends Controller
{
    /**
     * @param int $characters
     * @param int $strength
     * @return string
     * @throws \Exception
     */
    function generateStrongPassword(int $characters, int $strength): string
    {
        if ($characters < 6) $characters = 6;
        $sets = array();
        switch ($strength) {
            case 1:
                $sets[] = 'abcdefghjkmnpqrstuvwxyz';
                $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
                break;
            case 2:
                $sets[] = 'abcdefghjkmnpqrstuvwxyz';
                $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
                $sets[] = '123456789';
                break;
            case 3:
                $sets[] = 'abcdefghjkmnpqrstuvwxyz';
                $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
                $sets[] = '123456789';
                $sets[] = '!#$%&(){}[]=';
                break;
        }

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[$this->tweak_array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $characters; $i++)
            $password .= $all[$this->tweak_array_rand($all)];

        $password = str_shuffle($password);

        return $password;
    }

    /**
     * @param $array
     * @return array|int|string
     * @throws \Exception
     */
    function tweak_array_rand($array)
    {
        if (function_exists('random_int')) {
            return random_int(0, count($array) - 1);
        } elseif (function_exists('mt_rand')) {
            return mt_rand(0, count($array) - 1);
        } else {
            return array_rand($array);
        }
    }
}
