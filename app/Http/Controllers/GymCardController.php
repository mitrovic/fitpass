<?php

namespace App\Http\Controllers;

use App\Models\GymCard;
use App\Models\Visited;


class GymCardController extends Controller
{
    /**
     * @param $cardId
     * @param $objectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkLogin(int $cardId, int $objectId): \Illuminate\Http\JsonResponse
    {
        $result = GymCard::where('card_id', $cardId)
            ->where('gym_id', $objectId)
            ->firstOrFail();
        if ($result) {
            $card = $result->card;
            $user = $card->user->first();
            $gym = $result->gym->first();
            //check if is user already visited gym today
            if ($card->updated_at && $card->updated_at->isToday()) {
                $response = [
                    'status' => 'vec ste koristili danas karticu',
                ];
            } else {
                //update card
                $card->touch();
                //create new visit
                $visit = new Visited();
                $visit->gym_id = $objectId;
                $visit->user_id = $user->id;
                $visit->save();
                $response = [
                    'status' => 'OK',
                    'object_name' => $gym->name,
                    'first_name' => $user->firstname,
                    'last_name' => $user->lastname
                ];
            }
        }

        return response()->json($response);
    }
}
