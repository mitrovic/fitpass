<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGymCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gym_card', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('gym_id')->nullable();
            $table->unsignedBigInteger('card_id')->nullable();
            $table->foreign('gym_id')->references('id')->on('gym');
            $table->foreign('card_id')->references('id')->on('card');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gym_card');
    }
}
