<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visited', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('gym_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('gym_id')->references('id')->on('gym');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visited');
    }
}
