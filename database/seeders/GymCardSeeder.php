<?php
namespace Database\Seeders;

use App\Models\Card;
use App\Models\Gym;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class GymCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cards = Card::all();
        $gyms = Gym::all();
        foreach ($gyms as $gym) {
            DB::table('gym_card')->insert([
                'card_id'=>$cards->random()->id,
                'gym_id'=>$gym->id,
            ]);
        }
    }
}
