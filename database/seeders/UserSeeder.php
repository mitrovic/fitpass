<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            DB::table('users')->insert([
                'firstname' => $faker->name,
                'lastname' => $faker->lastName,
                'email' => $faker->email,
                'password' => bcrypt('secret'),
                'salt' => Str::random(10),
            ]);
        }
    }
}
