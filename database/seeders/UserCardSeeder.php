<?php
namespace Database\Seeders;

use App\Models\Card;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $cards = Card::all();
        foreach ($cards as $card) {
            DB::table('user_card')->insert([
                'user_id' => $users->random()->id,
                'card_id'=>$card->id,
            ]);
        }
    }
}
